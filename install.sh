#!/bin/bash

echo "Deploy configuration..."

cp bash.bashrc.sh /etc/bash.bashrc
cp bash.functions.sh /etc/bash.functions
cp bash.aliases.sh /etc/bash.aliases

echo "Install basics applications"
apt-get update -y
apt-get upgrade -y
apt-get install -y lm-sensors sudo git hg svn bzr htop iftop iotop curl nmap openssl

echo "Install Liquidprompt"
cd /opt/
git clone https://github.com/nojhan/liquidprompt.git

