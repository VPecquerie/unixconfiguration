export DOMAIN=vincent-p.fr

source /etc/bash.aliases
source /etc/bash.functions 
source /opt/liquidprompt/liquidprompt

if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

