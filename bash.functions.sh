function ban-ip {
  iptables -A INPUT -s $@ -j DROP
}


function docker-ip {
  docker inspect --format '{{ .NetworkSettings.IPAddress }}' "$@"
}

function fuck {
       command=$(echo `history |tail -n2 |head -n1` | sed 's/[0-9]* //')
       sudo $command
}


function go {
  if [ -z "$1"] # Hostname
  then
    if [ -z "$2"] # Username
    then
      if [ -z "$3"] # Port
      then
        if [ -z "$4"] # Domaine
        then
          $(echo ssh $2@$1.$4 -p$3)
        fi
      else
        $(echo ssh $2@$1.$DOMAIN)
      fi
    else
      $(echo ssh root@$1.$DOMAIN)
    fi
  else
    echo "Error : hostname isn't specified..."
  fi
}

function firewall-allow # firewall-allow tcp 21
{
  echo "iptables -t filter -A OUTPUT -p $1 --dport $2 -j ACCEPT" >> /etc/init.d/firewall
  echo "iptables -t filter -A INPUT -p $1 --dport $2 -j ACCEPT" >> /etc/init.d/firewall
}

function firewall-deny # firewall-deny tcp 21
{
  echo "iptables -t filter -A OUTPUT -p $1 --dport $2 -j DENY" >> /etc/init.d/firewall
  echo "iptables -t filter -A INPUT -p $1 --dport $2 -j DENY" >> /etc/init.d/firewall
}
