MASTER="5.39.91.108"
MASTERV6="2001:41d0:8:9d6c::1"
SLAVE="37.187.106.171"
SLAVEV6="2001:41d0:a:3aab::1"
SERIAL=$(date +%d%m%y%H%M)
REFRESH=3600

echo "zone \"$1\" {" >> /etc/bind/named.conf.local
echo "  type master;" >> /etc/bind/named.conf.local
echo "  allow-transfer { $SLAVE; } ;" >> /etc/bind/named.conf.local
echo "  file \"/etc/bind/db.$1\";" >> /etc/bind/named.conf.local
echo "};" >> /etc/bind/named.conf.local


echo '$TTL        604800'                                                      > /etc/bind/db.$1
echo "@        IN        SOA        $1. root.$1. ("                           >> /etc/bind/db.$1
echo "                        $SERIAL               ; Serial"                 >> /etc/bind/db.$1
echo "                        604800                ; Refresh"                >> /etc/bind/db.$1
echo "                         86400                ; Retry"                  >> /etc/bind/db.$1
echo "                       2419200                ; Expire"                 >> /etc/bind/db.$1
echo "                        604800 )              ; Negative Cache TTL"     >> /etc/bind/db.$1
echo ""                                                                       >> /etc/bind/db.$1
echo "                 IN      NS        master.$DOMAIN"                      >> /etc/bind/db.$1
echo "                 IN      NS        slave.$DOMAIN"                       >> /etc/bind/db.$1
echo "@       $REFRESH IN      A         $MASTER"                             >> /etc/bind/db.$1
echo "master  $REFRESH IN      A         $MASTER"                             >> /etc/bind/db.$1
echo "slave   $REFRESH IN      A         $SLAVE"                              >> /etc/bind/db.$1
echo "@       $REFRESH IN      AAAA      $MASTERV6"                           >> /etc/bind/db.$1
echo "master  $REFRESH IN      AAAA      $MASTERV6"                           >> /etc/bind/db.$1
echo "slave   $REFRESH IN      AAAA      $SLAVEV6"                            >> /etc/bind/db.$1

echo "mail    $REFRESH IN      A         $MASTER"                             >> /etc/bind/db.$1
echo "mail    $REFRESH IN      AAAA      $MASTERV6"                           >> /etc/bind/db.$1

echo "mail2   $REFRESH IN      A         $SLAVE"                              >> /etc/bind/db.$1
echo "mail2   $REFRESH IN      AAAA      $SLAVEV6"                            >> /etc/bind/db.$1

echo "dev     $REFRESH IN      A         $SLAVE"                              >> /etc/bind/db.$1
echo "dev     $REFRESH IN      AAAA      $SLAVEV6"                            >> /etc/bind/db.$1

echo "@       $REFRESH IN      MX    10  mail.$1."                            >> /etc/bind/db.$1
echo "@       $REFRESH IN      MX    20  mail2.$1."                           >> /etc/bind/db.$1

echo "*       $REFRESH IN      A         $MASTER"                             >> /etc/bind/db.$1
echo "*       $REFRESH IN      AAAA      $MASTERV6"                           >> /etc/bind/db.$1

if [ -d "/etc/opendkim/keys" ]; then
  cd /etc/opendkim/keys
  mkdir $1 && cd $1
  opendkim-genkey -s mail -d $1 -b 2048
  chown opendkim:opendkim mail.private
  KEY=$(cat mail.txt)

  echo "mail._domainkey IN TXT \"v=DKIM1; k=rsa; p=$KEY\""                     >> /etc/bind/db.$1
  echo "@        IN      TXT     \"v=spf1 mx a -all\""                         >> /etc/bind/db.$1

  service bind9    restart
  service postfix  restart
  service dovecot  restart
  service opendkim restart
fi
