#!/bin/bash
mkdir -p /sites/$1/{www,logs}
EMAILROOT=contact
WEBROOT=/sites/$1/www/
LOGROOT=/sites/$1/logs

cp defaultvhost  /etc/apache2/sites-available/$1.conf

sed -i "s|EMAILROOT|$EMAILROOT|g" /etc/apache2/sites-available/$1.conf
sed -i "s|WEBROOT|$WEBROOT|g" /etc/apache2/sites-available/$1.conf
sed -i "s|LOGROOT|$LOGROOT|g" /etc/apache2/sites-available/$1.conf

service apache2 reload
