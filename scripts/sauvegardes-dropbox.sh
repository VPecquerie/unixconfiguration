#!/bin/bash
#
timestamp=$(date +%d-%m-%Y_%Hh-%Mmn-%Ss)
BACKUP_DIR="/home/croangels/Dropbox/Sauvegardes/$timestamp/"

cd /tmp

MYSQL_USER="backup"
MYSQL=/usr/bin/mysql
MYSQL_PASSWORD="8T8v87tdaqA54nPs"
MYSQLDUMP=/usr/bin/mysqldump

mkdir $BACKUP_DIR
mkdir $BACKUP_DIR/www
mkdir $BACKUP_DIR/mysql

databases=`$MYSQL --user=$MYSQL_USER -p$MYSQL_PASSWORD -e "SHOW DATABASES;" | grep -Ev "(Database | information_schema | performance_schema)"`

for db in $databases; do
  echo "Sauvegarde de la base de données : $db..."
  $MYSQLDUMP --force --opt --user=$MYSQL_USER -p$MYSQL_PASSWORD --databases $db | gzip > "$BACKUP_DIR/mysql/$db.gz"
done

echo "Sauvegarde des sites web..."
cd /sites
find . -maxdepth 1 -mindepth 1 -type d -exec tar czf {}.tar.gz {}  \;
mv *.tar.gz $BACKUP_DIR/www/
